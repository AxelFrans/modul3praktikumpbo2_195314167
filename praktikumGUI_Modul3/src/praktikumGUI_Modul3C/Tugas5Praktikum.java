package praktikumGUI_Modul3C;

import javax.swing.*;
import java.awt.*;

public class Tugas5Praktikum extends JDialog {

    private JButton button_Left, button_Right;
    private JCheckBox centered, bold, italic;
    private JTextArea txt;
    private JRadioButton red, green, blue;
    private ButtonGroup button1;

    public Tugas5Praktikum() {

        this.setSize(320, 120);
        this.setTitle("RadioButtonDemo");
        
        this.setVisible(true);
        Container contentPane = getContentPane();
        txt = new JTextArea("Welcome to java");
        txt.setText(txt.getText());
        txt.setBackground(Color.WHITE);
        txt.setBounds(60, 1, 170, 60);
        txt.setLineWrap(true);
        contentPane.add(txt);

        centered = new JCheckBox("Centered");
        centered.setBounds(230, 1, 80, 20);
        bold = new JCheckBox("Bold");
        bold.setBounds(230, 19, 60, 20);
        italic = new JCheckBox("Italic");
        italic.setBounds(230, 39, 60, 20);
        contentPane.add(centered);
        contentPane.add(bold);
        contentPane.add(italic);

        button_Left = new JButton("Left");
        button_Left.setBounds(100, 65, 60, 20);
        button_Right = new JButton("Right");
        button_Right.setBounds(150, 65, 65, 20);
        this.add(button_Left);
        this.add(button_Right);

        button1 = new ButtonGroup();
        red = new JRadioButton("Red");
        red.setBounds(0, 1, 60, 20);
        this.add(red);
        green = new JRadioButton("Green");
        green.setBounds(0, 19, 60, 20);
        this.add(green);
        blue = new JRadioButton("Blue");
        blue.setBounds(0, 39, 60, 20);
        this.add(blue);
        button1.add(red);
        button1.add(green);
        button1.add(blue);

        JLabel label = new JLabel();
        this.add(label);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public static void main(String[] args) {
        Tugas5Praktikum RadioButton = new Tugas5Praktikum();
        RadioButton.setResizable(false);
    }

}
