package praktikumGUI_Modul3C;

import javax.swing.*;
import java.awt.*;

public class Tugas3Praktikum extends JDialog {

    private ImageIcon image;
    private JLabel label_Image;
    private JLabel icon;

    public Tugas3Praktikum() {
        this.setLayout(null);

        ImageIcon image = new ImageIcon(getClass().getResource("konoha.png"));

        label_Image = new JLabel(image);
        label_Image.setIcon(image);
        label_Image.setBounds(100, 20, 180, 180);
        this.add(label_Image);

        icon = new JLabel("Konoha");
        icon.setBounds(170, 210, 50, 20);
        this.add(icon);

    }

    public static void main(String[] args) {

        Tugas3Praktikum gui = new Tugas3Praktikum();
        gui.setSize(400, 320);
        gui.setVisible(true);
        gui.setResizable(false);
        gui.setTitle("Text and Icon Label");
        gui.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

}
