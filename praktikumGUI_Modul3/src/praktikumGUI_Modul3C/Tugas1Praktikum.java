package praktikumGUI_Modul3C;

import java.awt.*;
import javax.swing.*;

public class Tugas1Praktikum extends JFrame {

    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 450;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private JMenu menuFile;
    private JMenu menuEdit;
    private JMenuBar menuBar;

    public Tugas1Praktikum() {
        Container contentPane = getContentPane();
        contentPane.setBackground(Color.pink);

        menuFile = new JMenu("File");
        menuEdit = new JMenu("Edit");
        menuBar = new JMenuBar();
        menuBar.add(menuFile);
        menuBar.add(menuEdit);
        this.setJMenuBar(menuBar);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        Tugas1Praktikum frame = new Tugas1Praktikum();
        frame.setBounds(FRAME_X_ORIGIN, FRAME_Y_ORIGIN, FRAME_WIDTH, FRAME_HEIGHT);
        frame.setResizable(true);
        frame.setTitle("Frame Pertama");
        frame.setVisible(true);

    }

}
