package praktikumGUI_Modul3C;

import java.awt.*;
import javax.swing.*;

public class Tugas7Praktikum extends JDialog {

    private JList list_Country;
    private JLabel label_gambar;
    private ImageIcon USA_Flag, Germany_Flag, Canada_Flag, England_Flag, China_Flag, India_Flag;
    private JScrollPane scrollPane;

    public Tugas7Praktikum() {
        Container contentPane = getContentPane();
        this.setBounds(0, 0, 520, 270);
        this.setVisible(true);
        this.setTitle("ListDemo");
        contentPane.setBackground(Color.lightGray);
        contentPane.setLayout(null);

        list_Country = new JList(new String[]{"Canada", "China", "Denmark", "Frence", "Germany", "India", "Norway",
            "United Kingdom", "United States of America", "Israel", "Indonesia", "Australia", "Malaysia", "South Korea"});

        scrollPane = new JScrollPane(list_Country);
        scrollPane.setBounds(5, 10, 170, 220);
        this.add(scrollPane);

        Canada_Flag = new ImageIcon(getClass().getResource("Canada.png"));
        label_gambar = new JLabel(Canada_Flag);
        label_gambar.setBounds(185, 10, 100, 90);
        contentPane.add(label_gambar);
        China_Flag = new ImageIcon(getClass().getResource("China.png"));
        label_gambar = new JLabel(China_Flag);
        label_gambar.setBounds(290, 10, 100, 90);
        contentPane.add(label_gambar);
        Germany_Flag = new ImageIcon(getClass().getResource("Germany.png"));
        label_gambar = new JLabel(Germany_Flag);
        label_gambar.setBounds(395, 10, 100, 90);
        contentPane.add(label_gambar);
        India_Flag = new ImageIcon(getClass().getResource("India.png"));
        label_gambar = new JLabel(India_Flag);
        label_gambar.setBounds(185, 105, 100, 90);
        contentPane.add(label_gambar);

        England_Flag = new ImageIcon(getClass().getResource("England.jpg"));
        label_gambar = new JLabel(England_Flag);
        label_gambar.setBounds(290, 105, 100, 90);
        contentPane.add(label_gambar);

        USA_Flag = new ImageIcon(getClass().getResource("USA.png"));
        label_gambar = new JLabel(USA_Flag);
        label_gambar.setBounds(395, 105, 100, 90);
        contentPane.add(label_gambar);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    public static void main(String[] args) {
        Tugas7Praktikum listDemo = new Tugas7Praktikum();
        listDemo.setResizable(false);
    }

}
