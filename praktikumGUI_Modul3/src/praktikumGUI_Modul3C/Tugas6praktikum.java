package praktikumGUI_Modul3C;

import java.awt.*;
import javax.swing.*;

public class Tugas6praktikum extends JDialog {

    private JLabel label_gambar;
    private ImageIcon gambar;
    private JTextArea txt;
    private JComboBox ComboBox;
    private JScrollPane scrollPane;
    private JLabel name_Country;

    public Tugas6praktikum() {
        Container contentPane = getContentPane();
        this.setBounds(0, 0, 380, 180);
        this.setVisible(true);
        this.setTitle("ComboBoxDemo");
        contentPane.setBackground(Color.lightGray);
        contentPane.setLayout(null);

        scrollPane = new JScrollPane(new JTextArea());
        scrollPane.setBounds(200, 25, 170, 120);
        this.add(scrollPane);

        name_Country = new JLabel("Canada");
        name_Country.setBounds(75, 120, 50, 20);
        name_Country.setBackground(Color.lightGray);
        contentPane.add(name_Country);

        gambar = new ImageIcon(getClass().getResource("Canada small.png"));
        label_gambar = new JLabel(gambar);
        label_gambar.setBounds(1, 20, 200, 100);
        contentPane.add(label_gambar);

        ComboBox = new JComboBox(new String[]{"Canada", "England", "Rusia", "Frence"});
        ComboBox.setBounds(1, 1, 370, 19);
        contentPane.add(ComboBox);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    public static void main(String[] args) {
        Tugas6praktikum comboBox = new Tugas6praktikum();
        comboBox.setResizable(false);
    }

}
