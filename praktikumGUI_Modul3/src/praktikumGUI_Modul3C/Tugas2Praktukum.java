package praktikumGUI_Modul3C;

import java.awt.*;
import javax.swing.*;

public class Tugas2Praktukum extends JDialog {

    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 350;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;

    public Tugas2Praktukum() {

        this.setLayout(null);
        int i = 0;
        String warna[] = {"yellow", "blue", "red"};
        JButton yellow = new JButton(warna[i]);
        yellow.setBounds(40, 40, 80, 20);
        this.add(yellow);
        i++;
        JButton Blue = new JButton(warna[1]);
        Blue.setBounds(140, 40, 80, 20);
        this.add(Blue);
        i++;
        JButton red = new JButton(warna[2]);
        red.setBounds(240, 40, 80, 20);
        this.add(red);

    }

    public static void main(String[] args) {

        Tugas2Praktukum jDialog = new Tugas2Praktukum();
        jDialog.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        jDialog.setBackground(Color.YELLOW);
        jDialog.setVisible(true);
        jDialog.setTitle("Button Test");
        jDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }
}
