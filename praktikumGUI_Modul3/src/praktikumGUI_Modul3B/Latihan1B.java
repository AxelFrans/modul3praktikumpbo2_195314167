package praktikumGUI_Modul3B;

import java.awt.Color;
import javax.swing.JFrame;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JTextField;

public class Latihan1B extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField textField;

    public static void main(String[] args) {
        Latihan1B frame = new Latihan1B();
        frame.setVisible(true);

        
    }

    public Latihan1B() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Program Ch14AbsolutePositioning");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentPane.setLayout(null);
        contentPane.setBackground(Color.CYAN);

        okButton = new JButton("Ok");
        okButton.setBounds(110, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);

        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(160, 125, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
