package praktikumGUI_Modul3B;

import java.awt.Color;
import javax.swing.JFrame;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.Checkbox;
import javax.swing.JCheckBox;
public class Latihan1_InputData extends JFrame {

    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField textField;
   

    public Latihan1_InputData() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN , FRAME_Y_ORIGIN);

        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);
        
        JLabel labelNama = new JLabel("Nama :");
        labelNama.setBounds(10, 10, 100, 20);
        JLabel labelSex = new JLabel("Jenis Kelamin :");
        labelSex.setBounds(10, 40, 100, 20);
        JLabel labelHoby = new JLabel("Hoby :");
        labelHoby.setBounds(10, 70, 100, 20);
        
        
        this.add(labelNama);
        this.add(labelSex);
        this.add(labelHoby);
        
        JTextField fieldNama = new JTextField();
        fieldNama.setBounds(120, 10, 190, 20);
        this.add(fieldNama);
        
        JRadioButton buttonMan = new JRadioButton("Laki-Laki");
        buttonMan.setBounds(120, 40, 100, 20);
        this.add(buttonMan);
        JRadioButton buttonWoman = new JRadioButton("Perempuan");
        buttonWoman.setBounds(220, 40, 100, 20);
        this.add(buttonWoman);
        
        JCheckBox boxSport = new JCheckBox("Olahraga");
        boxSport.setBounds(120, 70, 100, 20);
        this.add(boxSport);
        JCheckBox boxShop = new JCheckBox("Shopping");
        boxShop.setBounds(120, 90, 100, 20);
        this.add(boxShop);
        JCheckBox boxGame = new JCheckBox("Computers Games");
        boxGame.setBounds(120, 110, 140, 20);
        this.add(boxGame);
        JCheckBox boxCinema = new JCheckBox("Nonton Bioskop");
        boxCinema.setBounds(120, 130, 140, 20);
        this.add(boxCinema);
        
        okButton = new JButton("Ok");
        okButton.setBounds(110, 170, 50, 20);
        contentPane.add(okButton);

        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(190, 170, 80, 20);
        contentPane.add(cancelButton);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        Latihan1_InputData xx = new Latihan1_InputData();
        xx.setVisible(true);
        
        
        
    }

}
